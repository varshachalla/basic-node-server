var express = require("express");
const graphqlHTTP = require('express-graphql');

var schema = require("./schema");

const app = express();

app.use('/graphql', graphqlHTTP({
  schema,
  graphiql: true
}));


let port = 5000;
app.listen(port, () => {
  console.log("Server is up and running on port numner " + port);
});
