exports.userData = [
  {
    id: 1,
    name: "Leanne Graham",
    username: "Bret",
    email: "Sincere@april.biz",
    phone: "1-770-736-8031 x56442",
    website: "hildegard.org"
  },
  {
    id: 2,
    name: "Ervin Howell",
    username: "Antonette",
    email: "Shanna@melissa.tv",
    phone: "010-692-6593 x09125",
    website: "anastasia.net"
  },
  {
    id: 3,
    name: "Clementine Bauch",
    username: "Samantha",
    email: "Nathan@yesenia.net",
    phone: "1-463-123-4447",
    website: "ramiro.info"
  },
  {
    id: 4,
    name: "Patricia Lebsack",
    username: "Karianne",
    email: "Julianne.OConner@kory.org",
    phone: "493-170-9623 x156",
    website: "kale.biz"
  },
  {
    id: 5,
    name: "Chelsey Dietrich",
    username: "Kamren",
    email: "Lucio_Hettinger@annie.ca",
    phone: "(254)954-1289",
    website: "demarco.info"
  },
  {
    id: 6,
    name: "Mrs. Dennis Schulist",
    username: "Leopoldo_Corkery",
    email: "Karley_Dach@jasper.info",
    phone: "1-477-935-8478 x6430",
    website: "ola.org"
  },
  {
    id: 7,
    name: "Kurtis Weissnat",
    username: "Elwyn.Skiles",
    email: "Telly.Hoeger@billy.biz",
    phone: "210.067.6132",
    website: "elvis.io"
  },
  {
    id: 8,
    name: "Nicholas Runolfsdottir V",
    username: "Maxime_Nienow",
    email: "Sherwood@rosamond.me",
    phone: "586.493.6943 x140",
    website: "jacynthe.com"
  },
  {
    id: 9,
    name: "Glenna Reichert",
    username: "Delphine",
    email: "Chaim_McDermott@dana.io",
    phone: "(775)976-6794 x41206",
    website: "conrad.com"
  },
  {
    id: 10,
    name: "Clementina DuBuque",
    username: "Moriah.Stanton",
    email: "Rey.Padberg@karina.biz",
    phone: "024-648-3804",
    website: "ambrose.net"
  }
];

exports.studentData = [
  {
    id: 1,
    name: "Lily cooper",
    examGrade: 3.2,
    homeworkGrade: 4.5
  },
  {
    id: 2,
    name: "Chris Evans",
    examGrade: 4.1,
    homeworkGrade: 5.0
  },
  {
    id: 3,
    name: "Chris Hemsworth",
    examGrade: 2.5,
    homeworkGrade: 4.9
  },
  {
    id: 4,
    name: "Safiya Nygard",
    examGrade: 4.2,
    homeworkGrade: 4.4
  },
  {
    id: 5,
    name: "Tom Riddle",
    examGrade: 1.0,
    homeworkGrade: 5.0
  },
  {
    id: 6,
    name: "Emma Watson",
    examGrade: 5.0,
    homeworkGrade: 5.0
  },
  {
    id: 7,
    name: "Jack Dawson",
    examGrade: 2.5,
    homeworkGrade: 3.0
  },
  {
    id: 8,
    name: "Jon Snow",
    examGrade: 3.5,
    homeworkGrade: 2.0
  },
  {
    id: 9,
    name: "Arya Stark",
    examGrade: 5.0,
    homeworkGrade: 5.0
  },
  {
    id: 10,
    name: "Jamie Lannister",
    examGrade: 2.5,
    homeworkGrade: 3.4
  }
];

exports.employeeData = [
  {
    userId: "rirani",
    jobTitleName: "Developer",
    firstName: "Romin",
    lastName: "Irani",
    preferredFullName: "Romin Irani",
    employeeCode: "E1",
    region: "CA",
    phoneNumber: "408-1234567",
    emailAddress: "romin.k.irani@gmail.com"
  },
  {
    userId: "nirani",
    jobTitleName: "Developer",
    firstName: "Neil",
    lastName: "Irani",
    preferredFullName: "Neil Irani",
    employeeCode: "E2",
    region: "CA",
    phoneNumber: "408-1111111",
    emailAddress: "neilrirani@gmail.com"
  },
  {
    userId: "thanks",
    jobTitleName: "Program Directory",
    firstName: "Tom",
    lastName: "Hanks",
    preferredFullName: "Tom Hanks",
    employeeCode: "E3",
    region: "CA",
    phoneNumber: "408-2222222",
    emailAddress: "tomhanks@gmail.com"
  },
  {
    userId: "ewatson",
    jobTitleName: "HR",
    firstName: "Emma",
    lastName: "Watson",
    preferredFullName: "Emma Watson",
    employeeCode: "E4",
    region: "LA",
    phoneNumber: "408-2452222",
    emailAddress: "emmawat@gmail.com"
  },
  {
    userId: "triddle",
    jobTitleName: "Program Directory",
    firstName: "Tom",
    lastName: "Riddle",
    preferredFullName: "Tom Riddle",
    employeeCode: "E5",
    region: "KS",
    phoneNumber: "548-7349284",
    emailAddress: "triddle@gmail.com"
  },
  {
    userId: "hPotter",
    jobTitleName: "Developer",
    firstName: "harry",
    lastName: "potter",
    preferredFullName: "harry potter",
    employeeCode: "E6",
    region: "London",
    phoneNumber: "643-987654",
    emailAddress: "theboywholived@gmail.com"
  },
  {
    userId: "Jmarsden",
    jobTitleName: "Tester",
    firstName: "James",
    lastName: "Marsden",
    preferredFullName: "James Marsden",
    employeeCode: "E7",
    region: "California",
    phoneNumber: "773-987654",
    emailAddress: "Bennyandthejets@gmail.com"
  },
  {
    userId: "charliec",
    jobTitleName: "Founder",
    firstName: "Charlie",
    lastName: "Puth",
    preferredFullName: "Charlie Puth",
    employeeCode: "E8",
    region: "Boston",
    phoneNumber: "233-987654",
    emailAddress: "charlieputh@gmail.com"
  },
  {
    userId: "squinn",
    jobTitleName: "Creative director",
    firstName: "Seb",
    lastName: "Quinn",
    preferredFullName: "Sebastian Quinn",
    employeeCode: "E9",
    region: "New York",
    phoneNumber: "100-987654",
    emailAddress: "sebQuinn@gmail.com"
  },
  {
    userId: "bettycoop",
    jobTitleName: "Writer",
    firstName: "betty",
    lastName: "Cooper",
    preferredFullName: "Betty Cooper",
    employeeCode: "E10",
    region: "Riverdale",
    phoneNumber: "100-64935",
    emailAddress: "bettycooper@gmail.com"
  }
];
