const graphql = require("graphql");

var data = require("./data");

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLList,
  GraphQLInt,
  GraphQLFloat
} = graphql;

const UserDataType = new GraphQLObjectType({
  name: "UserData",
  description: "User Data",
  fields: {
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
    username: { type: GraphQLString },
    phone: { type: GraphQLString },
    website: { type: GraphQLString }
  }
});

const StudentDataType = new GraphQLObjectType({
  name: "StudentData",
  description: "Student Data",
  fields: {
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
    examGrade: { type: GraphQLFloat },
    homeworkGrade: { type: GraphQLFloat }
  }
});
const EmployeeDataType = new GraphQLObjectType({
  name: "EmployeeData",
  description: "Employee Data",
  fields: {
    userId: { type: GraphQLString },
    jobTitleName: { type: GraphQLString },
    firstName: { type: GraphQLString },
    lastName: { type: GraphQLString },
    preferredFullName: { type: GraphQLString },
    employeeCode: { type: GraphQLString },
    region: { type: GraphQLString },
    phoneNumber: { type: GraphQLString },
    emailAddress: { type: GraphQLString }
  }
});

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    users: {
      type: new GraphQLList(UserDataType),
      resolve(parent, args) {
        return data.userData;
      }
    },
    students: {
      type: new GraphQLList(StudentDataType),
      resolve(parent, args) {
        return data.studentData;
      }
    },
    employees: {
        type: new GraphQLList(EmployeeDataType),
        resolve(parent, args) {
          return data.employeeData;
        }
      }
  }
});

module.exports = new GraphQLSchema({
  query: RootQuery
});
